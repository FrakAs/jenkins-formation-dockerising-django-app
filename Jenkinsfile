pipeline {
    agent {
        docker {
                image 'python:3.8'
                args '-u root:root'
            }
        }
    }
	environment {
        AUTHOR = 'Guillaume Rémy'
        PURPOSE    = 'This is a sample Django app'
        LIFE_QUOTE = 'The greatest glory in living lies not in never falling, but in rising every time we fall.'
    }
    stages {
        stage('Checkout') {
            steps {
                // Checkout your source code repository
                git branch: 'master',
                    url: 'https://gitlab.com/FrakAs/jenkins-formation-dockerising-django-app.git'
            }
        }
        stage('Install Dependencies') {
            steps {
                // Install required dependencies
                sh 'pip3 install -r requirements.txt' 
            }
        }
        stage('Test') {
            steps {
                // Run your Django tests
                sh 'python3 manage.py test'
            }
        }
        stage('Build') {
            environment {
                COMMIT_SHA = """${sh(
                    returnStdout: true,
                    script: "git log -n 1 --pretty=format:'%H'"
                )}"""
                DOCKER_CREDS = credentials('docker-harbor')
            }
            steps {
                // Build your Django application
                sh 'docker build -t harbor.jonctions.com/library/franck-pernet/django-simple-app:${COMMIT_SHA} .'
                sh 'docker login --username ${DOCKER_CREDS_USR} --password ${DOCKER_CREDS_PSW} harbor.jonctions.com'
                sh 'docker push harbor.jonctions.com/library/franck-pernet/django-simple-app:${COMMIT_SHA}'
            }
        }
        stage('Deploy') {
            environment {
                COMMIT_SHA = """${sh(
                    returnStdout: true,
                    script: "git log -n 1 --pretty=format:'%H'"
                )}"""
            }
            steps {
                // Deploy your Django application
                sh 'docker rm -f django-sample-app || true'
                sh 'docker run --name django-sample-app -d -p 8000:8000 -it -e AUTHOR -e PURPOSE -e LIFE_QUOTE harbor.jonctions.com/library/franck-pernet/django-simple-app:${COMMIT_SHA}'
            }
        }
    }
}
